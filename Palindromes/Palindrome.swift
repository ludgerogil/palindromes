//
//  Palindrome.swift
//  Palindromes
//
//  Created by Ludgero Mascarenhas on 12/11/18.
//  Copyright © 2018 Ludgero Mascarenhas. All rights reserved.
//

import UIKit
import RealmSwift

/// Classe para representar uma lista de objetos Palindromes no BD
class Palindrome: Object {
   
    @objc dynamic var word = ""
}
