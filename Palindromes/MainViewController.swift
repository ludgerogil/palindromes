//
//  MainViewController.swift
//  Palindromes
//
//  Created by Ludgero Mascarenhas on 12/11/18.
//  Copyright © 2018 Ludgero Mascarenhas. All rights reserved.
//

import UIKit
import RealmSwift

class MainViewController: UIViewController {
    
    // - MARK: Outlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var entradaTextField: UITextField!
    @IBOutlet weak var verificarButton: UIButton!
    
    // - MARK: Main
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        /// tap recognize na view para retirar o teclado
        let tapGesture = UITapGestureRecognizer(target: self,action: #selector(hideKeyboard))
        view.addGestureRecognizer(tapGesture)
        setUpAppereace()
        
        //print estado atual do banco
        //let realm = try! Realm()
        //let word = realm.objects(Palindrome.self)
        //print(word)
        
    }
    
    // - MARK: Layout da tela
    
    /// Função para setar os elementos da tela
    func setUpAppereace() {
        
        verificarButton.layer.borderWidth = 1.5
        verificarButton.layer.borderColor = UIColor.white.cgColor
        verificarButton.layer.cornerRadius = 5
    }
    
    // - MARK: Actions
    /// Action chama a funcao de palindrome com o texto do textfield
    @IBAction func verificar(_ sender: UIButton){
        
        guard let input = entradaTextField.text, input != "", input != " ", input != "  " else {
            print("entrada vazia")
            alert(msg: "String vazia")
            return
        }
        
        palindrome(string: input)
        
    }
    
    ///Função que esconde o teclado quando usuario clica na tela
    @objc func hideKeyboard() {
        view.endEditing(true)
    }
    
    
    // - MARK: Palindrome
    
    ///Função recebe uma string e chama isPalindrome para verficair se a string é palindrome ou não. Se for a palavra é guardada no banco.
    func palindrome(string: String) {
        
        let realm = try! Realm()
        
        var palavra = ""
        string.enumerateSubstrings(in: string.startIndex..<string.endIndex,
                                   options: .byWords) {
                                    (substring, _, _, _) -> () in
                                    palavra += substring ?? "erro"
        }
        
        if consultaBD(input: string) {
            alert(msg: "É palíndromo.")
        }
        else if (isPalindrome(string: palavra)) {
            let palavra = self.initPalavra(string)
            try! realm.write {
                realm.add(palavra)
            }
            alert(msg: "É palíndromo.")
        }
        else {
            alert(msg: "Não é palíndromo.")
        }
    }

    // - MARK: Funçoes da classe
    
    /// Função de classe que recebe uma string e retorna true se ela for palindrome
    fileprivate func isPalindrome(string: String) -> Bool {
        let characters = Array(string.lowercased())
        var index = 0
        
        while (index < characters.count/2) {
            if (characters[index] != characters[(characters.count - index - 1)]) {
                return false
            }
            index += 1
        }
        return true
    }
    
    ///Função para criar um objeto do tipo Palindrome
    fileprivate func initPalavra(_ word: String) -> Palindrome{
        let newWord = Palindrome()
        newWord.word = word
        return newWord
    }
    
    /// Recebe uma string e verifica se ela consta no Banco.
    fileprivate func consultaBD(input: String) -> Bool {
        
        var encontrou = false
        
        let realm = try! Realm()
        let palavra = realm.objects(Palindrome.self)
        
        for words in palavra {
            if (words.word == input) {
                encontrou = true
                break
            }
        }
        
        return encontrou
    }
    
    /// Funcão cria um alert com a msg se é palindrome ou não e apos o click do botao de ok o textfield é limpo.
    fileprivate func alert(msg: String) {
        let alert = UIAlertController(title: "Resposta", message: msg, preferredStyle: .alert)
        let ok = UIAlertAction(title:"Ok", style: .default, handler:{(UIAlertAction)-> Void in
            self.entradaTextField.text = ""
        })
        alert.addAction(ok)
        alert.view.tintColor = .black
        self.present(alert, animated: true)
       
    }

}

