# Palíndromo

O projeto foi implementado de forma nativa para iOS em Swift 4. A arquitetura utilizada na implementação foi MVC.

Foi utilizado o Cocoapods para gerenciar as dependências do projeto. O pod adicionado foi o SwiftRealm para criação de um banco de dados.

O código foi comentado em Markup.

### Aplicação

Aplicação recebe uma string qualquer (palavra, frase, ou qualquer sequência de caracteres) do usuário, via um campo de texto, e retorna na mesma tela se a String é um palíndromo ou não.
Há cada entrada do usuário verifica-se se a mesma já existe no Banco de Dados, se sim o usuário recebe a sua resposta, se não a string é testada. Se for palíndromo ela é adicionada ao Banco.

### Requisitos
Para a devida execução do projeto é necesário executar "pod install" via terminal (Se este já estiver instalado).

No Xcode é necessário abrir o Palindromes.xcworkspace.

### UI/UX
Foi feito uma UX e UI clean, utilizando auto-layout.

### Execução
A primeira tela da aplicação apresenta um campo de texto e um botão. Para utlizar basta clicar no campo de texto, digitar a string a ser testada e clicar no botão "Verificar".
Após é apresentado ao usuário um alert informando se a string é palíndromo ou não. 

